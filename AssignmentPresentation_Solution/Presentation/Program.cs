using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Http;

var builder = WebApplication.CreateBuilder(args);

System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", $"~/../app/dphomeas-d3e0dff2828d.json");
string projectName = builder.Configuration["project"];



builder.Services
    .AddAuthentication(options =>
    {
        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = GoogleDefaults.AuthenticationScheme;
    })
    .AddCookie()
    .AddGoogle(options =>
    {
        options.ClientId = "119004017355-fu05her56013f74q31dp93ap6dfugo70.apps.googleusercontent.com"; // builder.Configuration["Authentication:Google:ClientId"];
        options.ClientSecret = "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";// builder.Configuration["Authentication:Google:ClientSecret"];
    });


builder.Services.Configure<CookiePolicyOptions>(options =>
{
    options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
    options.OnAppendCookie = cookieContext =>
        CheckSameSite(cookieContext.CookieOptions);
    options.OnDeleteCookie = cookieContext =>
        CheckSameSite(cookieContext.CookieOptions);
});



// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.

}


app.UseCookiePolicy();

app.UseHsts();
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();


void CheckSameSite(CookieOptions options)
{
    if(options.SameSite == SameSiteMode.None)
    {
        options.SameSite = SameSiteMode.Unspecified;
    }
}