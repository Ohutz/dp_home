﻿using Common;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Presentation.API;
using Presentation.Models;
using System.Diagnostics;
using System.Net;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly UserAPI userApi = UserAPI.UserAPIInstance;
        private readonly CurrencyAPI currencyApi = CurrencyAPI.CurrencyAPIInstance;
        private readonly FundAccountAPI accountApi = FundAccountAPI.FundAccountAPIInstance;
        private readonly TwitterAPI twitterAPI = TwitterAPI.TwitterAPIInstance;
        private readonly AuditAPI auditAPI = AuditAPI.AuditAPIInstance;


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                List<FundAccount> result = accountApi.GetAccounts(User.Claims.ElementAt(4).Value);

                if (result.Count > 0)
                {
                    ViewBag.FundAccounts = result;
                }

                Preference preference = userApi.GetPreferences(User.Claims.ElementAt(4).Value);

                if(preference != null)
                {
                    //var exchangeRates = currencyApi.GetExchangeRates(User.Claims.ElementAt(4).Value);
                    //ViewBag.ExchangeRates = exchangeRates;

                    var tweets = twitterAPI.GetTweets(User.Claims.ElementAt(4).Value);
                    ViewBag.Tweets = tweets;
                }

                var transactions = auditAPI.GetRealtimeTransactions(User.Claims.ElementAt(4).Value);

                if(transactions != null)
                {
                    ViewBag.Transactions = transactions;
                }
                
                var exchangeRates = currencyApi.GetExchangeRates(User.Claims.ElementAt(4).Value);
                ViewBag.ExchangeRates = exchangeRates;
            }

            return View();
        }

        [Authorize]
        public async Task<IActionResult> Search([FromForm] string query)
        {
            List<FundAccount> result = accountApi.Search(User.Claims.ElementAt(4).Value, query);
            
            if (result.Count > 0)
            {
                ViewBag.FundAccounts = result;
            }
            else
            {
                TempData["warning"] = "Your search had no results.";
                return RedirectToAction("Index");
            }

            return View();
        }

        [Authorize]
        public async Task<IActionResult> Login()
        {
            if (userApi.Check(User.Claims.ElementAt(4).Value))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Register");
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Register(User user)
        {
            user.Email = User.Claims.ElementAt(4).Value;
            userApi.Register(user);
            return RedirectToAction("Preferences", "Users");
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Index");
        }


        [HttpGet]
        public async Task<IActionResult> ExchangeCalculator()
        {
            List<string> currencies = currencyApi.GetCurrencies();

            ViewBag.Currencies = currencies;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ExchangeCalculator(ExchangeItem item)
        {
            List<string> currencies = currencyApi.GetCurrencies();

            ViewBag.Currencies = currencies;

            float amount = currencyApi.GetExchangeValue(item.BaseCurrency, item.TargetCurrency, item.Amount);

            ViewData["CalculatedAmount"] = amount;


            return View();
        }

        #region irrelevant
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion
    }
}