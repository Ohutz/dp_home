﻿using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Presentation.API;
using System.Net;

namespace Presentation.Controllers
{
    [Authorize]
    public class FundsController : Controller
    {
        private readonly CurrencyAPI currencyApi = CurrencyAPI.CurrencyAPIInstance;
        private readonly FundAccountAPI accountApi = FundAccountAPI.FundAccountAPIInstance;
        private readonly TransactionAPI transactionApi = TransactionAPI.TransactionAPIInstance;
        private readonly AuditAPI auditApi = AuditAPI.AuditAPIInstance;


        [Authorize]
        public async Task<IActionResult> Index()
        {
            return RedirectToAction("Create");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Create()
        {
            FundAccount fundAccount = new FundAccount();
            fundAccount.Id = Guid.NewGuid().ToString();
            fundAccount.Balance = 0;

            List<string> currencies = currencyApi.GetCurrencies();

            ViewBag.currencies = currencies;
            

            return View(fundAccount);
        }
      
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(FundAccount fundAccount)
        {
            await accountApi.AddAccount(fundAccount, User.Claims.ElementAt(4).Value);

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public async Task<IActionResult> DisableAccount(string id)
        {
            accountApi.DisableAccount(User.Claims.ElementAt(4).Value, id);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> AddFunds(string id)
        {
            FundAccount account = accountApi.GetAccount(User.Claims.ElementAt(4).Value, id);

            return View(account);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddFunds(FundAccount account)
        {
            transactionApi.AddFunds(account, User.Claims.ElementAt(4).Value);

            return RedirectToAction("Index", "Home");
        }
        
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Transfer(string id)
        {
            Transfer transfer = new Transfer();

           
            List<FundAccount> result = accountApi.GetAccounts(User.Claims.ElementAt(4).Value);

            if (result.Count > 0)
            {
                ViewBag.FundAccounts = result;

                transfer.From = new FundAccount();
                transfer.From.Id = id;
            }
            else
            {
                ViewData["warning"] = "You need at least two fund accounts to make a transfer";
                return RedirectToAction("Index", "Home");
            }
            
            return View(transfer);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Transfer(Transfer transfer)
        {
            transfer.From = accountApi.GetAccount(User.Claims.ElementAt(4).Value, transfer.From.Id);
            transfer.To = accountApi.GetAccount(User.Claims.ElementAt(4).Value, transfer.To.Id);

            //Check if account has sufficient balance
            if (transfer.From.Balance >= transfer.Amount)
            {
                transfer.Id = Guid.NewGuid().ToString();
                transfer.Email = User.Claims.ElementAt(4).Value;

                var response = transactionApi.TransferFunds(transfer).Result;

                TempData["success"] = "Transfer Completed";
            }
            else
            {
                TempData["warning"] = "Insufficient Balance!";
            }

            return RedirectToAction("Index", "Home");
            
        }

        [Authorize]
        [HttpGet]
        public IActionResult Send(string id)
        {
            FundAccount account = new FundAccount();
            account.Id = id;
            return View(account);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Send(string id, string iban, string amount)
        {
            FundAccount account = accountApi.GetAccount(User.Claims.ElementAt(4).Value, id);

            if(account.Balance < float.Parse(amount))
            {
                TempData["warning"] = "Insufficient balance";
            }
            else
            {
                transactionApi.Send(id, User.Claims.ElementAt(4).Value, iban, amount);
                TempData["success"] = "Transfer completed";
            }

            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> AuditLog(string id)
        {
            if(accountApi.CheckIfOwner(User.Claims.ElementAt(4).Value, id))
            {
                TransactionsAndDeposits item = auditApi.GetTransactionsAndDeposits(User.Claims.ElementAt(4).Value, id);
                ViewBag.TransactionsAndDeposits = item;
                return View();
            }
            else
            {
                ViewData["warning"] = "Unauthorized access";

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AuditLog([FromForm] string id, [FromForm] string startDate, [FromForm] string endDate)
        {
            if(accountApi.CheckIfOwner(User.Claims.ElementAt(4).Value, id))
            {
                if (startDate != null && endDate != null)
                {
                    TransactionsAndDeposits item = auditApi.GetTransactionsAndDepositsByDate(User.Claims.ElementAt(4).Value, id, startDate, endDate);
                    ViewBag.TransactionsAndDeposits = item;
                    return View();
                }
                else
                {
                    return RedirectToAction($"AuditLog", new { id = id });
                }
            }
            else
            {
                TempData["warning"] = "Unauthorized access";
                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize]
        public async Task<IActionResult> Audit()
        {
            List<string> list = auditApi.GetAllTransactions(User.Claims.ElementAt(4).Value);

            if(list != null)
            {
                ViewBag.Transactions = list;
                return View();
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
