﻿using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Presentation.API;

namespace Presentation.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {

        private readonly UserAPI userApi = UserAPI.UserAPIInstance;
        private readonly CurrencyAPI currencyApi = CurrencyAPI.CurrencyAPIInstance;


        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Preferences()
        {
            List<string> currencies = currencyApi.GetCurrencies();
            ViewBag.currencies = currencies;

            Preference preference = userApi.GetPreferences(User.Claims.ElementAt(4).Value);

            return View(preference);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Preferences(List<string> prefCurrencies, string baseCurrency, string tweetsQuery)
         {
            List<string> currencies = currencyApi.GetCurrencies();
            ViewBag.currencies = currencies;

            Preference preference = new Preference();
            preference.Currencies = prefCurrencies;
            preference.BaseCurrency = baseCurrency;
            preference.TweetsQuery = tweetsQuery;

            var response = userApi.SetPreferences(preference, User.Claims.ElementAt(4).Value);

            if (response.Result.IsSuccessStatusCode)
            {
                TempData["success"] = "Preferences Updated";
            }
            else
            {
                ViewData["warning"] = "Failed to update preferences";
            }

            return View(preference);
        }

        [Authorize]
        public async Task<IActionResult> Profile()
        {
            User user = userApi.GetUser(User.Claims.ElementAt(4).Value);

            return View(user);
        }
    }
}
