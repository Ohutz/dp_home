﻿using Common;
using Presentation.Client;
using System.Text.Json;

namespace Presentation.API
{
    public class UserAPI
    {
        private readonly string hostEndpoint = "https://usermanagement-zoyx7a7tza-uc.a.run.app/api/v1/user";
        private readonly string registerEndpoint = "register";
        private readonly string checkEndpoint = "check";
        private readonly string getPreferencesEndpoint = "getpreferences";
        private readonly string setPreferencesEndpoint = "setpreferences";
        private readonly string getUserEndpoint = "getuser";

        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static UserAPI userAPIInstance = null;

        private UserAPI() { }

        public static UserAPI UserAPIInstance
        {
            get
            {
                if (userAPIInstance == null)
                {
                    userAPIInstance = new UserAPI();
                }

                return userAPIInstance;
            }
        }

        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{hostEndpoint}/{endpoint}/{queryString}")
            };

            return request;
        }

        public async void Register(User user)
        {
            string queryString = $"{user.Email}/{user.Name}/{user.Surname}";

            var request = GetRequestMessage(registerEndpoint, queryString);
            await webClient.Request(request);
        }

        public bool Check(string email)
        {
            string queryString = email;

            var request = GetRequestMessage(checkEndpoint, queryString);
            var result = webClient.Request(request).Result;

            if (result.RootElement.GetBoolean())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Preference GetPreferences(string email)
        {
            string queryString = email;

            var request = GetRequestMessage(getPreferencesEndpoint, queryString);
            var result = webClient.Request(request).Result;

            Preference preference = result.Deserialize<Preference>();

            return preference;
        }

        public async Task<HttpResponseMessage> SetPreferences(Preference preference, string email)
        {
            using(HttpClient client = new HttpClient())
            {
                var response = await client.PostAsJsonAsync($"{hostEndpoint}/{setPreferencesEndpoint}/{email}", preference);

                return response;
            }
        }
        
        public User GetUser(string email)
        {
            var queryString = email;

            var request = GetRequestMessage(getUserEndpoint, queryString);

            var response = webClient.Request(request).Result;

            User user = response.RootElement.Deserialize<User>();

            return user;
        }

    }
}
