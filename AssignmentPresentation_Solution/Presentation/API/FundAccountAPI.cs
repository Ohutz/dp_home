﻿using Common;
using Presentation.Client;
using System.Text.Json;

namespace Presentation.API
{
    public class FundAccountAPI
    {
        private readonly string hostEndpoint = "https://fundaccountmanagement-zoyx7a7tza-uc.a.run.app/api/v1/account";
        private readonly string addAccountEndpoint = "addaccount";
        private readonly string getAccountsEndpoint = "getaccounts";
        private readonly string getAccountEndpoint = "getaccount";
        private readonly string searchEndpoint = "search";
        private readonly string disableAccountEndpoint = "disableaccount";
        private readonly string checkIfOwnerEndpoint = "checkifowner";

        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static FundAccountAPI fundAccountAPIInstance = null;
        private FundAccountAPI() {}

        public static FundAccountAPI FundAccountAPIInstance
        {
            get
            {
                if(fundAccountAPIInstance == null)
                {
                    fundAccountAPIInstance = new FundAccountAPI();
                }

                return fundAccountAPIInstance;
            }
        }

        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{hostEndpoint}/{endpoint}/{queryString}")
            };

            return request;
        }

        public async Task<HttpResponseMessage> AddAccount(FundAccount account, string email)
        {
            using(HttpClient client = new HttpClient())
            {
                var response = await client.PostAsJsonAsync($"{hostEndpoint}/{addAccountEndpoint}/{email}", account);

                return response;
            }
        }

        public List<FundAccount> GetAccounts(string email)
        {
            string queryString = email;
            
            var request = GetRequestMessage(getAccountsEndpoint, queryString);

            var result = webClient.Request(request).Result;

            List<FundAccount> list = result.Deserialize<List<FundAccount>>();

            return list;
        }

        public FundAccount GetAccount(string email, string id)
        {
            string queryString = $"{email}/{id}";

            var request = GetRequestMessage(getAccountEndpoint, queryString);

            var result = webClient.Request(request).Result;

            FundAccount account = result.Deserialize<FundAccount>();

            return account;
        }

        public List<FundAccount> Search(string email, string query)
        {
            string queryString = $"{email}/{query}";

            var request = GetRequestMessage(searchEndpoint, queryString);

            var result = webClient.Request(request).Result;

            List<FundAccount> list = result.Deserialize<List<FundAccount>>();

            return list;
        }

        public async void DisableAccount(string email, string id)
        {
            string queryString = $"{email}/{id}";

            var request = GetRequestMessage(disableAccountEndpoint, queryString);

            await webClient.Request(request);
        }

        public bool CheckIfOwner(string email, string id)
        {
            string queryString = $"{email}/{id}";
            var request = GetRequestMessage(checkIfOwnerEndpoint, queryString);
            var result = webClient.Request(request).Result;

            return result.Deserialize<bool>();
        }
    }
}
