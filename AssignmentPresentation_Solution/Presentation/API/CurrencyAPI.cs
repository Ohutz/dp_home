﻿using Presentation.Client;
using System.Text.Json;

namespace Presentation.API
{
    public class CurrencyAPI
    {
        private readonly string hostEndpoint = "https://currencyconverterservice-zoyx7a7tza-uc.a.run.app/api/v1/currency";
        private readonly string getCurrenciesEndpoint = "getcurrencies";
        private readonly string getExchangeAmountEndpoint = "getexchangeamount";
        private readonly string getExchangeRatesEndpoint = "getexchangerates";

        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static CurrencyAPI currencyAPIInstance;

        private CurrencyAPI() { }

        public static CurrencyAPI CurrencyAPIInstance
        {
            get
            {
                if(currencyAPIInstance == null)
                {
                    currencyAPIInstance = new CurrencyAPI();
                }

                return currencyAPIInstance;
            }
        }

        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{hostEndpoint}/{endpoint}/{queryString}")
            };

            return request;
        }

        public List<string> GetCurrencies()
        {
            var request = GetRequestMessage(getCurrenciesEndpoint, null);

            var result = webClient.Request(request).Result;

            var json = result.RootElement;

            var list = json.Deserialize<List<string>>();

            return list;
        }

        public float GetExchangeValue(string baseCurrency, string targetCurrency, float amount)
        {
            string queryString = $"{baseCurrency}/{targetCurrency}/{amount}";

            var request = GetRequestMessage(getExchangeAmountEndpoint, queryString);

            var result = webClient.Request(request).Result;

            double value = result.RootElement.GetDouble();

            return (float)value;
        }

        public List<string> GetExchangeRates(string email)
        {
            var request = GetRequestMessage(getExchangeRatesEndpoint, email);
            var result = webClient.Request(request).Result;
            var rootElement = result.RootElement;

            List<string> rates = rootElement.Deserialize<List<string>>();

            return rates;
        }
    }
}
