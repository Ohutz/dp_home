﻿using Common;
using Presentation.Client;

namespace Presentation.API
{
    public class TransactionAPI
    {
        private readonly string hostEndpoint = "https://transactionmanagement-zoyx7a7tza-uc.a.run.app/api/v1/transaction";
        private readonly string addFundsEndpoint = "addfunds";
        private readonly string sendEndpoint = "send";

        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static TransactionAPI transactionAPIInstance = null;
        private TransactionAPI() { }

        public static TransactionAPI TransactionAPIInstance
        {
            get
            {
                if(transactionAPIInstance == null)
                {
                    transactionAPIInstance = new TransactionAPI();
                }

                return transactionAPIInstance;
            }
        }

        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{hostEndpoint}/{endpoint}/{queryString}")
            };

            return request;
        }

        public void AddFunds(FundAccount account, string email)
        {
            string queryString = $"{email}/{account.Id}/{account.Balance}";

            var request = GetRequestMessage(addFundsEndpoint, queryString);

            var b = webClient.Request(request).Result;
        }


        public bool Send(string id, string email, string IBAN, string amount)
        {
            string queryString = $"{id}/{email}/{IBAN}/{amount}";

            var request = GetRequestMessage(sendEndpoint, queryString);

            var result = webClient.Request(request);

            if (result.IsCompletedSuccessfully)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
        public async Task<bool> TransferFunds(Transfer transfer)
        {
            using(HttpClient client = new HttpClient())
            {
                var result = await client.PostAsJsonAsync($"{hostEndpoint}", transfer);

                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
