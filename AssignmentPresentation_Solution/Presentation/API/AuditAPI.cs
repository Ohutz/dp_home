﻿using Common;
using Newtonsoft.Json;
using Presentation.Client;
using System.Text.Json;


namespace Presentation.API
{
    public class AuditAPI
    {
        private readonly string hostEndpoint = "https://auditlogservice-zoyx7a7tza-uc.a.run.app/api/v1/audit"; //"https://localhost:7088/api/v1/audit"; 
        private readonly string getAllTransactionsEndpoint = "getalltransactions";
        private readonly string getRealtimeTransactionsEndpoint = "getrealtimetransactions";
        private readonly string getTransactionsAndDepositsEndpoint = "gettransactionsanddeposits";
        private readonly string getTransactionsAndDepositsByDateEndpoint = "GetTransfersByDate";


        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static AuditAPI auditApiInstance = null;
        private AuditAPI() { }

        public static AuditAPI AuditAPIInstance
        {
            get
            {
                if (auditApiInstance == null)
                {
                    auditApiInstance = new AuditAPI();
                }

                return auditApiInstance;
            }
        }

        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{hostEndpoint}/{endpoint}/{queryString}")
            };

            return request;
        }

        public List<string> GetRealtimeTransactions(string email)
        {
            var request = GetRequestMessage(getRealtimeTransactionsEndpoint, email);

            var result = webClient.Request(request).Result;

            var json = result.RootElement;

            var list = json.Deserialize<List<string>>();

            if(list.Count != 0)
            {
                return list;
            }

            return null;
        }

        public TransactionsAndDeposits GetTransactionsAndDeposits(string email, string id)
        {
            var queryString = $"{email}/{id}";

            var request = GetRequestMessage(getTransactionsAndDepositsEndpoint, queryString);

            var result = webClient.Request(request).Result;

            var json = result.RootElement;

            var item = json.Deserialize<TransactionsAndDeposits>();

            return item;
        }

        public TransactionsAndDeposits GetTransactionsAndDepositsByDate(string email, string id, string startDate, string endDate)
        {
            var queryString = $"{email}/{id}/{System.Web.HttpUtility.UrlEncode(startDate)}/{System.Web.HttpUtility.UrlEncode(endDate)}";

            var request = GetRequestMessage(getTransactionsAndDepositsByDateEndpoint, queryString);

            var result = webClient.Request(request).Result;

            var json = result.RootElement;

            var item = json.Deserialize<TransactionsAndDeposits>();

            return item;
        }

        public List<string> GetAllTransactions(string email)
        {
            var request = GetRequestMessage(getAllTransactionsEndpoint, email);
            
            var result = webClient.Request(request).Result;

            var json = result.RootElement;

            var item = json.Deserialize<List<string>>();

            return item;

        }
    }
}
