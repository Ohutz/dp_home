﻿using Presentation.Client;
using System.Text.Json;

namespace Presentation.API
{
    public class TwitterAPI
    {
        private readonly string hostEndpoint = "https://twitterservice-zoyx7a7tza-uc.a.run.app/api/v1/twitter";
        private readonly string getTweetsEndpoint = "gettweets";

        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static TwitterAPI twitterAPIInstance = null;

        public static TwitterAPI TwitterAPIInstance
        {
            get
            {
                if(twitterAPIInstance == null)
                {
                    twitterAPIInstance = new TwitterAPI();
                }

                return twitterAPIInstance;
            }
        }

        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(hostEndpoint + "/" + endpoint + "/" + queryString),

            };

            return request;
        }

        public List<string> GetTweets(string email)
        {
            var request = GetRequestMessage(getTweetsEndpoint, email);

            var response = webClient.Request(request).Result;

            List<string> result = response.RootElement.Deserialize<List<string>>();

            return result;
        }

    }
}
