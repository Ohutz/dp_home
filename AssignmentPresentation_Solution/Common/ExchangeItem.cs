﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ExchangeItem
    {
        public string BaseCurrency { get; set; }

        public string TargetCurrency { get; set; }

        public float Amount { get; set; }

    }
}
