﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Deposit
    {
        public float Amount { get; set; }

        public Timestamp Date { get; set; }
        
        public DateTime DateTime { get; set; }
    }
}
