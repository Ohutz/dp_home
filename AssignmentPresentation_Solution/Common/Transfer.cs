﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Transfer
    {
        public string Id { get; set; }

        public FundAccount From { get; set; }

        public FundAccount To { get; set; }

        public string Email { get; set; }

        public float Amount { get; set; }
    }
}
