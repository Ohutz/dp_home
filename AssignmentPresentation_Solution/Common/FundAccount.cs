﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class FundAccount
    {
        public string Id { get; set; }

        [Required]
        public string BankAccountNo { get; set; }

        [Required]
        public string IBAN { get; set; }

        [Required]
        public string Payee { get; set; }

        [Required]
        public string BankName { get; set; }

        [Required]
        public string BankCode { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string CurrencyCode { get; set; }

        [Required]
        public float Balance { get; set; }

        public int IsDisabled { get; set; } = 0;
    }
}
