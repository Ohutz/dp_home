﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class TransactionsAndDeposits
    {
        public FundAccount Account { get; set; }

        public List<Deposit> Deposits { get; set; }

        public List<TransferItem> TransferItems { get; set; }

    }
}
