﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Preference
    {
        [Required]
        public string BaseCurrency { get; set; }

        [Required]
        public List<string> Currencies { get; set; }

        public string TweetsQuery { get; set; }
    }
}
