﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interfaces
{
    public interface IPubsubRepository
    {
        void PublishTwitter(string tag);

        Task<string> PublishExchangeRates(List<ExchangeItem> items);
    }
}
