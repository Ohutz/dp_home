﻿using Common;
using DataAccess.Interfaces;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class PubsubRepository : IPubsubRepository
    {
        private string projectId;

        public PubsubRepository(string projId)
        {
            projectId = projId;
        }

        public async void PublishTwitter(string tag)
        {
            TopicName topic = new TopicName(projectId, "as_dp_twitter_topic");
            PublisherClient client = PublisherClient.Create(topic);

            var data = JsonConvert.SerializeObject(tag);


            PubsubMessage message = new PubsubMessage()
            {
                Data = ByteString.CopyFromUtf8(data)
            };

            var result = await client.PublishAsync(message);
        }

        public async Task<string> PublishExchangeRates(List<ExchangeItem> items)
        {
            TopicName topic = new TopicName(projectId, "as_dp_exchange_rates_topic");
            PublisherClient client = PublisherClient.Create(topic);

            var data = JsonConvert.SerializeObject(items);

            PubsubMessage message = new PubsubMessage()
            {
                Data = ByteString.CopyFromUtf8(data)
            };

            var result = await client.PublishAsync(message);

            return "";
        }
    }
}
