using CloudNative.CloudEvents;
using Google.Cloud.Firestore;
using Google.Cloud.Functions.Framework;
using Google.Events.Protobuf.Cloud.PubSub.V1;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Twitter
{

    [FirestoreData]
    public class TweetsList{
        [FirestoreProperty]
        public List<string> Tweets {get; set;}
    }

    public class Function : ICloudEventFunction<MessagePublishedData>
    {
        private readonly ILogger _logger;
        //private readonly string apiKey = "skLKvyYfxGcVrdfjO2Vu1hQOf";
        private readonly string bearerToken = "AAAAAAAAAAAAAAAAAAAAAIuMbgEAAAAAJZZ1KfTYNqRQtitj2n9MLEPSDqI%3DZ5eofHlAxfKa7EmzljjoPIYmseR0o0N5PSfj6olG6f5QBJRcVM";
        private readonly string hostEndpoint = "https://api.twitter.com/2";
        private readonly string tweetsEndpoint = "tweets";

        public Function(ILogger<Function> logger) =>
            _logger = logger;



        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString){
            var request = new HttpRequestMessage{
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{hostEndpoint}/{endpoint}/{queryString}")
            };
            return request;
        }

        public async Task<JsonDocument> Request(HttpRequestMessage requestMessage, string token)
        {
            string responseValue;

            var client = new HttpClient();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            using (var response = await client.SendAsync(requestMessage))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                responseValue = body;
            }

            _logger.LogInformation($"Response: {responseValue}");

            return JsonDocument.Parse(responseValue);
        }

        public Task HandleAsync(CloudEvent cloudEvent, MessagePublishedData data, CancellationToken cancellationToken)
        {
            try
            {

                _logger.LogInformation("Accessed Twitter cloud function");
                string json = data.Message?.TextData;
                _logger.LogInformation($"Recieved json: {json}");

                dynamic obj = JsonConvert.DeserializeObject(json);

                string email = obj.Email;
                string tag = obj.Tag;


                List<string> tweets = new List<string>();

                _logger.LogInformation("Creating request");
                var request = GetRequestMessage(tweetsEndpoint, $"search/recent?query={tag}");
                _logger.LogInformation("Sending request");
                var result = Request(request, bearerToken).Result;
                _logger.LogInformation("Recieved response");

                if (result.RootElement.TryGetProperty("data", out JsonElement dataElement))
                {
                    foreach (var item in dataElement.EnumerateArray())
                    {
                        if (item.TryGetProperty("text", out JsonElement text))
                        {
                            tweets.Add(text.ToString());
                        }
                    }
                }


                if (tweets != null)
                {
                    _logger.LogInformation("Uploading to firestore");
                    FirestoreDb db = FirestoreDb.Create("dphomeas");

                    DocumentReference docRef = db.Collection("users").Document(email).Collection("temp").Document("tweets_temp");

                    TweetsList tweetsList = new TweetsList();
                    tweetsList.Tweets = tweets;

                    var awaitresult = docRef.SetAsync(tweetsList).Result;

                    _logger.LogInformation("Done");

                    _logger.LogInformation("Cloud function executed successfully.");
                    return Task.FromResult(awaitresult);
                }
            }catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
