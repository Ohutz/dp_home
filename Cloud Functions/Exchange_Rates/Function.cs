using CloudNative.CloudEvents;
using Google.Cloud.Firestore;
using Google.Cloud.Functions.Framework;
using Google.Events.Protobuf.Cloud.PubSub.V1;
using Google.Events.Protobuf.Cloud.Storage.V1;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Exchange_Rates
{
   [FirestoreData]
    public class RatesList{
        [FirestoreProperty]
        public List<string> Rates {get; set;}
    }

    public class Preference{
        public string BaseCurrency {get; set;}

        public List<string> Currencies {get; set;}

        public string TweetsQuery {get; set;}
    }

    public class PreferenceWrapper{
        public string Email {get; set;}

        public Preference Preference {get; set;}
    }


    public class Function : ICloudEventFunction<MessagePublishedData>
    {
        private readonly ILogger _logger;
        private readonly string hostEndpoint = "https://currencyconverterservice-zoyx7a7tza-uc.a.run.app/api/v1/currency";
        private readonly string getExchangeValueEndpoint = "GetExchangeAmount";

        public Function(ILogger<Function> logger) =>
            _logger = logger;
        

        public Task HandleAsync(CloudEvent cloudEvent, MessagePublishedData data, CancellationToken cancellationToken)
        {
            try{
                _logger.LogInformation("Accessed exchange rates function");
                string payload = data.Message?.TextData;
                _logger.LogInformation($"Recieved payload: {payload}");

                _logger.LogInformation($"Deserializing payload..");
                PreferenceWrapper obj = JsonConvert.DeserializeObject<PreferenceWrapper>(payload);
                _logger.LogInformation("Deserialized");
                string baseCurrency = obj.Preference.BaseCurrency;
                _logger.LogInformation(baseCurrency);
                List<string> currencies = obj.Preference.Currencies;
                _logger.LogInformation("Currencies obtained");
                string email = obj.Email;
                _logger.LogInformation($"Email: {email}");

                RatesList rates = new RatesList();
                rates.Rates = new List<string>();

                using (HttpClient client = new HttpClient()){
                    foreach(var targetcurrency in currencies){
                        _logger.LogInformation($"Base currency: {baseCurrency}, target currency: {targetcurrency}");
                        
                        string url = $"{hostEndpoint}/{getExchangeValueEndpoint}/{baseCurrency}/{targetcurrency}/1";
                        _logger.LogInformation($"Full url: {url}");

                        var result = client.GetAsync(url).Result;
                        _logger.LogInformation("Recieved response");

                        string exchangeAmount = result.Content.ReadAsStringAsync().Result;

                        

                        rates.Rates.Add($"{baseCurrency} to {targetcurrency}: {exchangeAmount}");
                        _logger.LogInformation($"Exchange Amount: {exchangeAmount}");
                    }    
                }

                _logger.LogInformation($"Adding to firebase");

                FirestoreDb db = FirestoreDb.Create("dphomeas");
                DocumentReference docRef = db.Collection("users").Document(email).Collection("temp").Document("rates_temp");

                var awaitresult = docRef.SetAsync(rates).Result;

                _logger.LogInformation("Done");
                _logger.LogInformation("Cloud function exited successfully");
                
                return Task.FromResult(awaitresult);

            }catch(Exception ex){
                _logger.LogError(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
