using CloudNative.CloudEvents;
using Google.Cloud.Firestore;
using Google.Cloud.Functions.Framework;
using Google.Events.Protobuf.Cloud.PubSub.V1;
using Google.Events.Protobuf.Cloud.Storage.V1;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Transactions
{
    [FirestoreData]
    public class TransactionList
    {
        [FirestoreProperty]
        public List<string> Transactions { get; set; }

        public string Email { get; set; }
    }

    public class Function : ICloudEventFunction<MessagePublishedData>
    {

        public readonly ILogger _logger;


        public Function(ILogger<Function> logger) =>
            _logger = logger;

        public Task HandleAsync(CloudEvent cloudEvent, MessagePublishedData data, CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation("Accessed Cloud function");
                string payload = data.Message?.TextData;
                _logger.LogInformation($"Recieved payload: {payload}");

                _logger.LogInformation("Deserializing payload");
                TransactionList transactionList = JsonConvert.DeserializeObject<TransactionList>(payload);
                _logger.LogInformation("Deserialized");

                _logger.LogInformation("Adding to firestore");
                FirestoreDb db = FirestoreDb.Create("dphomeas");
                DocumentReference docRef = db.Collection("users").Document(transactionList.Email).Collection("temp").Document("transactions_temp");

                var awaitresult = docRef.SetAsync(transactionList).Result;
                _logger.LogInformation("Done");
            }catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
