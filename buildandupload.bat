
docker build -f ./Assignment_solution/AuditLogService/Dockerfile -t gcr.io/dphomeas/auditlogservice .
docker build -f ./Assignment_solution/CurrencyConverter/Dockerfile -t gcr.io/dphomeas/currencyconverter .
docker build -f ./Assignment_solution/FundAccountManagement/Dockerfile -t gcr.io/dphomeas/fundaccountmanagement .
docker build -f ./Assignment_solution/TransactionManagement/Dockerfile -t gcr.io/dphomeas/transactionmanagement .
docker build -f ./Assignment_solution/TwitterService/Dockerfile -t gcr.io/dphomeas/twitterservice .
docker build -f ./Assignment_solution/UserManagement/Dockerfile -t gcr.io/dphomeas/usermanagement .

cd ./AssignmentPresentation_solution

docker build -t gcr.io/dphomeas/presentation .

docker push gcr.io/dphomeas/auditlogservice
docker push gcr.io/dphomeas/currencyconverter
docker push gcr.io/dphomeas/fundaccountmanagement
docker push gcr.io/dphomeas/transactionmanagement
docker push gcr.io/dphomeas/twitterservice
docker push gcr.io/dphomeas/usermanagement
docker push gcr.io/dphomeas/presentation
