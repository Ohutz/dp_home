﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Common;
using DataAccess.Interfaces;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace FundAccountManagement.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IFirestoreDataAccess _firestore;

        public AccountController(IFirestoreDataAccess firestore)
        {
            _firestore = firestore;
        }

        [HttpGet("CheckIfOwner/{email}/{id}")]
        public bool CheckIfOwner(string email, string id)
        {
            return _firestore.CheckIfOwner(email, id).Result;
        }

        [HttpPost("AddAccount/{email}")]
        public void AddAccount(FundAccount content, string email)
        {
            _firestore.AddFundAccount(content, email);
        }

        [HttpGet("GetAccounts/{email}")]
        public string GetFundAccounts(string email)
        {
            var list = _firestore.GetFundAccounts(email).Result;
            var json = JsonConvert.SerializeObject(list);

            return json;
        }

        [HttpGet("GetAccount/{email}/{id}")]
        public async Task<string> GetFundAccount(string email, string id)
        {
            var account = await _firestore.GetFundAccount(email, id);
            var json = JsonConvert.SerializeObject(account);

            return json;
        }

        //Convert to json?
        [HttpGet("Search/{email}/{query}")]
        public async Task<string> Search(string email, string query)
        {
            List<FundAccount> list = await _firestore.SearchFundAccounts(email, query);
            var json = JsonConvert.SerializeObject(list);

            return json;
        }

        [HttpGet("DisableAccount/{email}/{id}")]
        public bool DisableFundAccount(string email, string id)
        {
            _firestore.DisableFundAccount(email, id);

            return true;
        }
    }
}
