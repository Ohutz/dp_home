using DataAccess.Interfaces;
using DataAccess.Repositories;

var builder = WebApplication.CreateBuilder(args);

System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", $"dphomeas-d3e0dff2828d.json");

string projectName = builder.Configuration["project"];

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



builder.Services.AddScoped<IFirestoreDataAccess, FirestoreDataAccess>(x =>
{
    return new FirestoreDataAccess(projectName);
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
