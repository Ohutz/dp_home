﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Interfaces
{
    public interface IFirestoreDataAccess
    {
        void AddUser(User user);
       
        void AddFundAccount(FundAccount fundAccount, string email);

        void SetPreferences(Preference preference, string email);

        Task<Preference> GetPreferences(string email);

        void DisableFundAccount(string email, string id);

        Task<bool> CheckIfOwner(string email, string id);

        void AddFunds(string email, string id, float amount);

        Task<List<FundAccount>> SearchFundAccounts(string email, string query);

        Task<Tuple<FundAccount, string>> GetTPFundAccount(string iban);

        void TransferFundsTP(Transfer transfer, string toEmail);

        void TransferFunds(Transfer transfer);

        void UpdateUser(User user);

        Task<User> GetUser(string email);
        Task<List<User>> GetAllUsers();

        Task<List<FundAccount>> GetFundAccounts(string email);

        Task<FundAccount> GetFundAccount(string email, string id);

        Task<List<string>> GetAllTransactions(string email);

        Task<List<TransferItem>> GetTransactions(string email, string accountId);

        Task<List<Deposit>> GetDeposits(string email, string accountId);

        Task<List<string>> GetRealtimeTransactions(string email);

        Task<List<string>> GetTweets(string email);

        Task<List<string>> GetExchangeRates(string email);
    }
}
