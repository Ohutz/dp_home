﻿using Common;
using DataAccess.Interfaces;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class PubsubRepository : IPubsubRepository
    {
        private string projectId;

        public PubsubRepository(string projId)
        {
            projectId = projId;
        }

        public void PublishTwitter(string tag, string email)
        {
            TopicName topic = new TopicName(projectId, "as_dp_twitter_topic");
            PublisherClient client = PublisherClient.Create(topic);


            dynamic dataobj = new { Tag = tag, Email = email };

            var data = JsonConvert.SerializeObject(dataobj);

            

            PubsubMessage message = new PubsubMessage()
            {
                Data = ByteString.CopyFromUtf8(data)
            };

            client.PublishAsync(message);
        }

        
        public void PublishExchangeRates(Preference preference, string email)
        {
            TopicName topic = new TopicName(projectId, "as_dp_exchange_rates_topic");
            PublisherClient client = PublisherClient.Create(topic);

            dynamic obj = new { Preference = preference, Email = email };

            var data = JsonConvert.SerializeObject(obj);

            PubsubMessage message = new PubsubMessage()
            {
                Data = ByteString.CopyFromUtf8(data)
            };

            client.PublishAsync(message);
        }

        public void PublishTransactions(List<string> transactions, string email)
        {
            TopicName topic = new TopicName(projectId, "as_dp_transactions_topic");
            PublisherClient client = PublisherClient.Create(topic);

            dynamic obj = new { Transactions = transactions, Email = email };

            var data = JsonConvert.SerializeObject(obj);

            PubsubMessage message = new PubsubMessage()
            {
                Data = ByteString.CopyFromUtf8(data)
            };

            client.PublishAsync(message);
        }
    }
}
