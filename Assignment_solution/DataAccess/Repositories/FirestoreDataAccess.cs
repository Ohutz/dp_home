﻿using Common;
using DataAccess.Interfaces;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class FirestoreDataAccess : IFirestoreDataAccess
    {
        private FirestoreDb db { get; set; }

        public FirestoreDataAccess(string project)
        {
            db = FirestoreDb.Create(project);
        }

        public async void AddUser(User user)
        {
            DocumentReference docRef = db.Collection("users").Document(user.Email);

            await docRef.SetAsync(user);
        }

        public async void AddFundAccount(FundAccount fundAccount, string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("accounts").Document(fundAccount.Id);

            await docRef.SetAsync(fundAccount);
        }

        public async void DisableFundAccount(string email, string id)
        {
            await db.Collection("users").Document(email).Collection("accounts").Document(id).UpdateAsync("IsDisabled", 1);
        }

        public async Task<bool> CheckIfOwner(string email, string id)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("accounts").Document(id);
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();
            
            FundAccount account = snapshot.ConvertTo<FundAccount>();


            if (account != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<User> GetUser(string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email);
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            if (snapshot.Exists)
            {
                User myUser = snapshot.ConvertTo<User>();

                return myUser;
            }
            else
            {
                return null;
            }
        }

        public async Task<List<User>> GetAllUsers()
        {
            Query query = db.Collection("users");
            QuerySnapshot snapshot = await query.GetSnapshotAsync();

            List<User> users = new List<User>();

            foreach(DocumentSnapshot documentSnapshot in snapshot)
            {
                var user = documentSnapshot.ConvertTo<User>();
                users.Add(user);
            }

            return users;
        }

        public async void SetPreferences(Preference preference, string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("preferences").Document("prefs");

            await docRef.SetAsync(preference);
        }

        public async Task<Preference> GetPreferences(string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("preferences").Document("prefs");
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            if (snapshot.Exists)
            {
                Preference preference = snapshot.ConvertTo<Preference>();

                return preference;
            }
            else
            {
                return null;
            }

        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public async Task<List<FundAccount>> GetFundAccounts(string email)
        {
            Query query = db.Collection("users").Document(email).Collection("accounts");
            QuerySnapshot snapshot = await query.GetSnapshotAsync();

            List<FundAccount> result = new List<FundAccount>();

            foreach(DocumentSnapshot documentSnapshot in snapshot)
            {
                var acc = documentSnapshot.ConvertTo<FundAccount>();

                if(acc.IsDisabled == 0)
                {
                    result.Add(acc);
                }
               
            }

            return result;
        }

        public async Task<FundAccount> GetFundAccount(string email, string id)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("accounts").Document(id);
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            FundAccount result = snapshot.ConvertTo<FundAccount>();

            return result;
        }

        public async Task<List<FundAccount>> SearchFundAccounts(string email, string query)
        {
            var accounts = await GetFundAccounts(email);

            var filteredList = accounts.Where(x => x.BankAccountNo.Contains(query)).ToList();

            return filteredList;
        }

        public async Task<Tuple<FundAccount, string>> GetTPFundAccount(string iban)
        {
            Query query = db.Collection("users");
            QuerySnapshot snapshot = await query.GetSnapshotAsync();

            foreach(DocumentSnapshot documentSnapshot in snapshot)
            {
                User userSnap = documentSnapshot.ConvertTo<User>();

                Query secondQuery = db.Collection("users").Document(userSnap.Email).Collection("accounts");
                QuerySnapshot secondSnapshot = await secondQuery.GetSnapshotAsync();

                foreach(DocumentSnapshot secondDocumentSnapshot in secondSnapshot)
                {
                    var acc = secondDocumentSnapshot.ConvertTo<FundAccount>();

                    if(acc.IBAN == iban)
                    {
                        return new Tuple<FundAccount, string>(acc, userSnap.Email);
                    }
                }
            }

            return null;
        }

        /// <summary>
        ///     NEEDS REFACTORING!!!!
        /// </summary>
        /// <param name="transfer"></param>
        /// <param name="toEmail"></param>
        public async void TransferFundsTP(Transfer transfer, string toEmail)
        {
            float exchangeAmount = 0;

            transfer.From.Balance -= transfer.Amount;


            if (transfer.From.CurrencyCode.Equals(transfer.To.CurrencyCode))
            {
                exchangeAmount = transfer.Amount;
                transfer.To.Balance += transfer.Amount;
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    string exchangeAmountString = await client.GetStringAsync(
                        $"https://currencyconverterservice-zoyx7a7tza-uc.a.run.app/api/v1/Currency/GetExchangeAmount/{transfer.From.CurrencyCode}/{transfer.To.CurrencyCode}/{transfer.Amount}"
                    );

                    float.TryParse(exchangeAmountString, out exchangeAmount);

                    //2DP
                    exchangeAmount = (float)(Math.Truncate((double)exchangeAmount * 100.0) / 100.0);

                    transfer.To.Balance += exchangeAmount;
                }
            }


            TransferItem fromTransfer = new TransferItem();
            TransferItem toTransfer = new TransferItem();

            fromTransfer.Id = transfer.Id;
            fromTransfer.From_IBAN = transfer.From.IBAN;
            fromTransfer.To_IBAN = transfer.To.IBAN;
            fromTransfer.Balance = "-" + transfer.Amount + " " + transfer.From.CurrencyCode;

            toTransfer.Id = transfer.Id;
            toTransfer.From_IBAN = transfer.From.IBAN;
            toTransfer.To_IBAN= transfer.To.IBAN;
            toTransfer.Balance = "+" + exchangeAmount + " " + transfer.To.CurrencyCode;

            DocumentReference fromRef = db.Collection("users").Document(transfer.Email).Collection("accounts").Document(transfer.From.Id).Collection("transfers").Document(fromTransfer.Id); ;
            DocumentReference toRef = db.Collection("users").Document(toEmail).Collection("accounts").Document(transfer.To.Id).Collection("transfers").Document(toTransfer.Id);

            await fromRef.SetAsync(fromTransfer);
            await toRef.SetAsync(toTransfer);

            AddFundAccount(transfer.From, transfer.Email); //update
            AddFundAccount(transfer.To, toEmail);
        }

        public async void AddFunds(string email, string id, float amount)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("accounts").Document(id);
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            FundAccount account = snapshot.ConvertTo<FundAccount>();

            await db.Collection("users").Document(email).Collection("accounts").Document(id).UpdateAsync("Balance", account.Balance + amount);

            string guid = Guid.NewGuid().ToString();

            DocumentReference docRef2 = db.Collection("users").Document(email).Collection("accounts").Document(id).Collection("deposits").Document(guid);

            Deposit deposit = new Deposit();
            deposit.Amount = amount;

            docRef2.SetAsync(deposit);

        }

        public async void AddTransfer(Transfer transfer, float exAmount)
        {
            TransferItem fromTransfer = new TransferItem();
            TransferItem toTransfer = new TransferItem();

            
            fromTransfer.Id = transfer.Id;
            fromTransfer.From_IBAN = transfer.From.IBAN;
            fromTransfer.To_IBAN = transfer.To.IBAN;
            fromTransfer.Balance = "-" + transfer.Amount + " " + transfer.From.CurrencyCode;

            toTransfer.Id = transfer.Id;
            toTransfer.From_IBAN = transfer.From.IBAN;
            toTransfer.To_IBAN = transfer.To.IBAN;
            toTransfer.Balance = "+" + exAmount + " " + transfer.To.CurrencyCode;

            DocumentReference fromRef = db.Collection("users").Document(transfer.Email).Collection("accounts").Document(transfer.From.Id).Collection("transfers").Document(fromTransfer.Id); ;
            DocumentReference toRef = db.Collection("users").Document(transfer.Email).Collection("accounts").Document(transfer.To.Id).Collection("transfers").Document(toTransfer.Id);

            await fromRef.SetAsync(fromTransfer);
            await toRef.SetAsync(toTransfer);
        }

        public async void TransferFunds(Transfer transfer)
        {
            float exchangeAmount = 0;
            
            transfer.From.Balance -= transfer.Amount;


            if (transfer.From.CurrencyCode.Equals(transfer.To.CurrencyCode))
            {
                exchangeAmount = transfer.Amount;
                transfer.To.Balance += transfer.Amount;
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    string exchangeAmountString = await client.GetStringAsync(
                        $"https://currencyconverterservice-zoyx7a7tza-uc.a.run.app/api/v1/Currency/GetExchangeAmount/{transfer.From.CurrencyCode}/{transfer.To.CurrencyCode}/{transfer.Amount}"
                    );

                    float.TryParse(exchangeAmountString, out exchangeAmount);

                    transfer.To.Balance += exchangeAmount;
                }
            }

            AddFundAccount(transfer.From, transfer.Email); //update
            AddFundAccount(transfer.To, transfer.Email); //update
            AddTransfer(transfer, exchangeAmount); //add transfer to accounts
        }

        public async Task<List<string>> GetAllTransactions(string email)
        {
            List<string> transactions = new List<string>();

            Query accountQuery = db.Collection("users").Document(email).Collection("accounts");
            QuerySnapshot accountQuerySnapshot = await accountQuery.GetSnapshotAsync();

            foreach(DocumentSnapshot snapshot in accountQuerySnapshot)
            {
                Query transactionQuery = db.Collection("users").Document(email).Collection("accounts").Document(snapshot.Id).Collection("transfers");
                QuerySnapshot transactionQuerySnapshot = await transactionQuery.GetSnapshotAsync();

                foreach(DocumentSnapshot transaction in transactionQuerySnapshot)
                {
                    var transfer = transaction.ConvertTo<TransferItem>();
                    transactions.Add($"{transfer.DateSent} From {transfer.From_IBAN} To {transfer.To_IBAN}: {transfer.Balance}");              
                }
                    
            }

            return transactions;
        }

        public async Task<List<TransferItem>> GetTransactions(string email, string accountId)
        {
            List<TransferItem> transfers = new List<TransferItem>();

            Query transactionsQuery = db.Collection("users").Document(email).Collection("accounts").Document(accountId).Collection("transfers");
            QuerySnapshot snapshot = await transactionsQuery.GetSnapshotAsync();

            foreach(DocumentSnapshot docSnapshot in snapshot)
            {
                TransferItem transfer = docSnapshot.ConvertTo<TransferItem>();
                transfer.DateTime = transfer.DateSent.ToDateTime();

                transfers.Add(transfer);
            }

            return transfers;
        }

        public async Task<List<Deposit>> GetDeposits(string email, string accountId)
        {
            List<Deposit> deposits = new List<Deposit>();

            Query transactionsQuery = db.Collection("users").Document(email).Collection("accounts").Document(accountId).Collection("deposits");
            QuerySnapshot snapshot = await transactionsQuery.GetSnapshotAsync();

            foreach (DocumentSnapshot docSnapshot in snapshot)
            {
                Deposit deposit = docSnapshot.ConvertTo<Deposit>();
                deposit.DateTime = deposit.Date.ToDateTime();

                deposits.Add(deposit);
            }

            return deposits;
        }

        public async Task<List<string>> GetRealtimeTransactions(string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("temp").Document("transactions_temp");
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            RealtimeTransactionsList transactionList = snapshot.ConvertTo<RealtimeTransactionsList>();


            if (transactionList != null)
            {
                return transactionList.Transactions;
            }

            return null;
        }

        public async Task<List<string>> GetTweets(string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("temp").Document("tweets_temp");
            DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();

            var tweets = snapshot.ConvertTo<TweetsList>();

            if(tweets != null)
            {
                return tweets.Tweets.Take(5).ToList();
            }

            return null;
        }

        public async Task<List<string>> GetExchangeRates(string email)
        {
            DocumentReference docRef = db.Collection("users").Document(email).Collection("temp").Document("rates_temp");
            DocumentSnapshot documentSnapshot = await docRef.GetSnapshotAsync();

            RatesList ratesList = documentSnapshot.ConvertTo<RatesList>();

            return ratesList.Rates;
        }
    }
}
