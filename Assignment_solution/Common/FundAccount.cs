﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [FirestoreData]
    public class FundAccount
    {
        [FirestoreProperty]
        public string Id { get; set; }

        [FirestoreProperty]
        public string BankAccountNo { get; set; }

        [FirestoreProperty]
        public string IBAN { get; set; }

        [FirestoreProperty]
        public string Payee { get; set; }

        [FirestoreProperty]
        public string BankName { get; set; }

        [FirestoreProperty]
        public string BankCode { get; set; }

        [FirestoreProperty]
        public string Country { get; set; }

        [FirestoreProperty]
        public string CurrencyCode { get; set; }

        [FirestoreProperty]
        public float Balance { get; set; }

        [FirestoreProperty]
        public int IsDisabled { get; set; }
    }
}
