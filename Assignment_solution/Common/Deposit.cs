﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common
{

    [FirestoreData]
    public class Deposit
    {
        [FirestoreProperty]
        public float Amount { get; set; }

        [FirestoreProperty, ServerTimestamp]
        public Timestamp Date { get; set; }

        public DateTime DateTime { get; set; }

    }
}
