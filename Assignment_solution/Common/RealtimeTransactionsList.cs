﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [FirestoreData]
    public class RealtimeTransactionsList
    {
        [FirestoreProperty]
        public List<string> Transactions { get; set; }
    }
}
