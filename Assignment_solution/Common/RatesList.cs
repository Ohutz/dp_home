﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [FirestoreData]
    public class RatesList
    {
        [FirestoreProperty]
        public List<string> Rates { get; set; } 
    }
}
