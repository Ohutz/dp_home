﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [FirestoreData]
    public class TweetsList
    {
        [FirestoreProperty]
        public List<string> Tweets { get; set; }
    }
}
