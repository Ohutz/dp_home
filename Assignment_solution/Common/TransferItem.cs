﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{   
    [FirestoreData]
    public class TransferItem
    {
        [FirestoreProperty]
        public string Id { get; set; }
        
        [FirestoreProperty]
        public string From_IBAN { get; set; }

        [FirestoreProperty]
        public string To_IBAN { get; set; }

        [FirestoreProperty]
        public string Balance { get; set; }

        [FirestoreProperty, ServerTimestamp]
        public Timestamp DateSent { get; set; }

        public DateTime DateTime { get; set; }
    }
}
