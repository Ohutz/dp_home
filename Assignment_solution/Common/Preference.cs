﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [FirestoreData]
    public class Preference
    {
        [FirestoreProperty]
        [Required]
        public string BaseCurrency { get; set; }

        [FirestoreProperty]
        [Required]
        public List<string> Currencies { get; set; }

        [FirestoreProperty]
        [Required]
        public string TweetsQuery { get; set; }

    }
}
