﻿using Common;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;

namespace UserManagement.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IFirestoreDataAccess _firestore;

        public UserController(IFirestoreDataAccess firestore)
        {
            _firestore = firestore;
        }

        [HttpGet("Register/{email}/{firstName}/{lastName}")]
        public bool Register(string email, string firstName, string lastName)
        {
            if (_firestore.GetUser(email).Result == null)
            {
                User user = new User();
                user.Email = email;
                user.Name = firstName;
                user.Surname = lastName;
                _firestore.AddUser(user);

                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpGet("Check/{email}")]
        public bool CheckIfExists(string email)
        {
            if (_firestore.GetUser(email).Result == null)
            {
                return false;
            }

            return true;
        }

        [HttpGet("GetPreferences/{email}")]
        public async Task<string> GetPreferences(string email)
        {
            var pref = await _firestore.GetPreferences(email);

            string json = JsonConvert.SerializeObject(pref);

            return json;
        }

        [HttpPost("SetPreferences/{email}")]
        public HttpResponseMessage SetPreferences(Preference payload, string email)
        {
            _firestore.SetPreferences(payload, email);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpGet("GetUser/{email}")]
        public async Task<string> GetUser(string email)
        {
            var json = JsonConvert.SerializeObject(await _firestore.GetUser(email));

            return json;
        }
    }
}
