﻿using Common;
using DataAccess.Interfaces;
using Google.Cloud.Firestore;
using Google.Type;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AuditLogService.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuditController : ControllerBase
    {
        private readonly IFirestoreDataAccess _firestore;
        private readonly IPubsubRepository _pubsub;

        public AuditController(IFirestoreDataAccess firestore, IPubsubRepository pubsub)
        {
            _pubsub = pubsub;
            _firestore = firestore;
        }

        [HttpGet("GetAllTransactions/{email}")]
        public async Task<string> GetAllTransactions(string email)
        {
            var transactions = await _firestore.GetAllTransactions(email);

            string json = JsonConvert.SerializeObject(transactions);

            return json;
        }


        [HttpGet("GetTransactionsAndDeposits/{email}/{id}")]
        public async Task<string> GetTransactionsAndDeposits(string email, string id) {

            FundAccount account = await _firestore.GetFundAccount(email, id);

            TransactionsAndDeposits tdItem = new TransactionsAndDeposits();
            tdItem.TransferItems = await _firestore.GetTransactions(email, account.Id);
            tdItem.Deposits = await _firestore.GetDeposits(email, account.Id);

            tdItem.Account = account;


            var json = JsonConvert.SerializeObject(tdItem);

            return json;

        }

        [HttpGet("GetTransfersByDate/{email}/{id}/{startDate}/{endDate}")]
        public async Task<string> GetTransfersByDate(string email, string id, string startDate, string endDate) 
        {
            TransactionsAndDeposits filteredItem = new TransactionsAndDeposits();

            var json = await GetTransactionsAndDeposits(email, id);
            TransactionsAndDeposits tdItem = JsonConvert.DeserializeObject<TransactionsAndDeposits>(json);

            System.DateTime start = Convert.ToDateTime(System.Web.HttpUtility.UrlDecode(startDate));
            System.DateTime end = Convert.ToDateTime(System.Web.HttpUtility.UrlDecode(endDate));

            
            List<Deposit> deposits = new List<Deposit>();

            foreach(Deposit deposit in tdItem.Deposits)
            {
                if(deposit.DateTime >= start && deposit.DateTime <= end)
                {
                    deposits.Add(deposit);
                }
            }

            List<TransferItem> transfers = new List<TransferItem>();
                
            foreach(TransferItem transfer in tdItem.TransferItems)
            {
                if(transfer.DateTime >= start && transfer.DateTime <= end)
                {
                    transfers.Add(transfer);
                }
            }

            TransactionsAndDeposits transactionsAndDeposits = new TransactionsAndDeposits();
            transactionsAndDeposits.TransferItems = transfers;
            transactionsAndDeposits.Deposits = deposits;
            transactionsAndDeposits.Account = tdItem.Account;

            filteredItem = transactionsAndDeposits;
            

            var jsonReturn = JsonConvert.SerializeObject(filteredItem);

            return jsonReturn;
        }


        [HttpGet("GetRealtimeTransactions/{email}")]
        public async Task<string> GetRealtimeTransactions(string email)
        {
            List<string> transactions = await _firestore.GetRealtimeTransactions(email);

            if(transactions == null)
            {
                transactions = new List<string>(); 
            }

            string json = JsonConvert.SerializeObject(transactions);
            return json;
        }

        [HttpGet("Cron")]
        public IActionResult Cron()
        {
            List<User> users = _firestore.GetAllUsers().Result;

            foreach (User user in users)
            {
                var transactions = _firestore.GetAllTransactions(user.Email).Result;
                List<string> transactionsCropped = transactions.Take(5).ToList();
                _pubsub.PublishTransactions(transactionsCropped, user.Email);
            }

            return Ok("Done");
        }
    }
}
