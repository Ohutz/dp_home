﻿using Common;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace TransactionManagement.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase 
    {
        private readonly IFirestoreDataAccess _firestore;

        public TransactionController(IFirestoreDataAccess firestore)
        {
            _firestore = firestore;
        }

        [HttpGet("AddFunds/{email}/{id}/{amount}")]
        public bool AddFunds(string email, string id, float amount)
        {
            _firestore.AddFunds(email, id, amount);

            return true;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> TransferFunds(Transfer transfer)
        {
             _firestore.TransferFunds(transfer);

            HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.Created);

            return message;
        }

        [HttpGet("Send/{fromId}/{email}/{iban}/{amount}")]
        public async Task<HttpResponseMessage> Send(string fromId, string email, string iban, string amount)
        {
            FundAccount fromAcc = await _firestore.GetFundAccount(email, fromId);
            var accTup = await _firestore.GetTPFundAccount(iban);
            FundAccount toAcc = accTup.Item1;
            string toEmail = accTup.Item2;


            if (toAcc != null)
            {
                Transfer transfer = new Transfer();
                transfer.Id = Guid.NewGuid().ToString();
                transfer.From = fromAcc;
                transfer.To = toAcc;
                transfer.Email = email;
                transfer.Amount = float.Parse(amount);

                _firestore.TransferFundsTP(transfer, toEmail);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
