﻿using Common;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TwitterService.API;

namespace TwitterService.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TwitterController : ControllerBase
    {
        private readonly IPubsubRepository _pubsub;
        private readonly IFirestoreDataAccess _firestore;

        public TwitterController(IPubsubRepository pubsub, IFirestoreDataAccess firestore)
        {
            _pubsub = pubsub;
            _firestore = firestore;
        }

        [HttpGet("GetTweets/{email}")]
        public string GetTweets(string email)
        {
            var tweets = _firestore.GetTweets(email).Result;

            string json;

            if(tweets == null)
            {
                tweets = new List<string>();
            }

            json = JsonConvert.SerializeObject(tweets);
            return json;
        }

        //Runs every minute.
        //This method will call the cloud function for each user and the cloud function will 
        //store the tweets in firestore per user object
        [HttpGet("Cron")]
        public IActionResult Cron()
        {
            List<User> users = _firestore.GetAllUsers().Result;

            foreach(User user in users)
            {
                Preference preference = _firestore.GetPreferences(user.Email).Result;

                if(preference != null && preference.TweetsQuery!= null)
                {
                    _pubsub.PublishTwitter(preference.TweetsQuery, user.Email);
                }
                
            }

            return Ok("Done");
        }
    }
}
