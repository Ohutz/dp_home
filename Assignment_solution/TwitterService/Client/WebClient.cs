﻿using System.Net.Http.Headers;
using System.Text.Json;

namespace TwitterService.Client
{
    public class WebClient
    {
        private static WebClient webClientInstance = null;

        private WebClient() { }

        public static WebClient WebClientInstance
        {
            get
            {
                if (webClientInstance == null)
                {
                    webClientInstance = new WebClient();
                }

                return webClientInstance;
            }
        }

        public async Task<JsonDocument> Request(HttpRequestMessage requestMessage, string token)
        {
            string responseValue;

            var client = new HttpClient();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            using (var response = await client.SendAsync(requestMessage))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                responseValue = body;
            }

            return JsonDocument.Parse(responseValue);
        }
    }
}
