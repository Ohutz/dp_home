﻿using DataAccess.Interfaces;
using System.Text.Json;
using TwitterService.Client;

namespace TwitterService.API
{
    public class TwitterAPI
    {
        private IPubsubRepository _pubsub;
        private IFirestoreDataAccess _firestore;

        private TwitterAPI(IPubsubRepository pubsub, IFirestoreDataAccess firestore) {
            _pubsub = pubsub;
            _firestore = firestore;
        }

    }
}
