﻿using Common;
using CurrencyConverter.API;
using DataAccess.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CurrencyConverter.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly CurrencyAPI currencyApi = CurrencyAPI.CurrencyAPIInstance;
        private readonly IPubsubRepository _pubsub;
        private readonly IFirestoreDataAccess _firestore;

        public CurrencyController(IPubsubRepository pubsub, IFirestoreDataAccess firestore)
        {
            _pubsub = pubsub;
            _firestore = firestore;
        }

        [HttpGet("GetCurrencies")]
        public string GetCurrencies()
        {
            return JsonConvert.SerializeObject(currencyApi.GetAvailableCurrencies());
        }

        [HttpGet("GetExchangeAmount/{baseCurrency}/{targetCurrency}/{amount}")]
        public float GetExchangeValue(string baseCurrency, string targetCurrency, float amount)
        {
            string queryString = "&symbols=" + baseCurrency;

            var exchangeRateFromEURtoBase =  currencyApi.GetExchangeValue(queryString, baseCurrency, 1);
            var inverseRate = 1 / exchangeRateFromEURtoBase;
            var tempAmount = amount * inverseRate;

            queryString = "&symbols=" + targetCurrency;

            return currencyApi.GetExchangeValue(queryString, targetCurrency, tempAmount);
        }

        [HttpGet("GetExchangeRates/{email}")]
        public string GetExchangeRates(string email)
        {
            var list = _firestore.GetExchangeRates(email).Result;

            string json = JsonConvert.SerializeObject(list);

            return json;
        }

        [HttpGet("Cron")]
        public IActionResult Cron()
        {
            List<User> users = _firestore.GetAllUsers().Result;

            foreach(User user in users)
            {
                Preference preference = _firestore.GetPreferences(user.Email).Result;

                if(preference != null && preference.BaseCurrency != null && preference.Currencies != null)
                {
                    _pubsub.PublishExchangeRates(preference, user.Email);
                }
            }

            return Ok("Done");
        }
    }
}
