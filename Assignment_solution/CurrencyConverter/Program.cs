using DataAccess.Interfaces;
using DataAccess.Repositories;

System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", $"dphomeas-d3e0dff2828d.json");

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IPubsubRepository, PubsubRepository>(x =>
{
    return new PubsubRepository("dphomeas");
});

builder.Services.AddScoped<IFirestoreDataAccess, FirestoreDataAccess>(x =>
{
    return new FirestoreDataAccess("dphomeas");
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
