﻿using Assignment.Client;
using Common;
using Newtonsoft.Json;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace CurrencyConverter.API
{
    public class CurrencyAPI
    {
        private readonly string hostEndpoint = "http://api.exchangeratesapi.io/v1";
        private readonly string latestEndpoint = "latest";
        private readonly string symbolsEndpoint = "symbols";
        private readonly string apiKey = "b8afde21e6f627e575799ee05dc9164c";


        private readonly WebClient webClient = WebClient.WebClientInstance;

        private static CurrencyAPI currencyAPIInstance = null;

        private CurrencyAPI() { }

        public static CurrencyAPI CurrencyAPIInstance
        {
            get
            {
                if (currencyAPIInstance == null)
                {
                    currencyAPIInstance = new CurrencyAPI();
                }

                return currencyAPIInstance;
            }
        }


        private HttpRequestMessage GetRequestMessage(string endpoint, string queryString)
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(hostEndpoint + "/" + endpoint + "?access_key=" + apiKey + queryString),

            };

            return request;
        }

        public List<string> GetAvailableCurrencies()
        {
            List<string> availableCurrencies = new List<string>();

            var request = GetRequestMessage(symbolsEndpoint, null);
            var result = webClient.Request(request).Result;


            if(result.RootElement.TryGetProperty("symbols", out JsonElement symbolsElement))
            {
                string json = symbolsElement.ToString();
                var symbolsDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                foreach(var key in symbolsDictionary.Keys)
                {
                    availableCurrencies.Add(key);
                }
            }

            return availableCurrencies;
        }

        public float GetExchangeValue(string queryString, string symbol, float amount)
        {
            float exchangeRate = 0;

            var request = GetRequestMessage(latestEndpoint, queryString);
            var result = webClient.Request(request).Result;
            
            if(result.RootElement.TryGetProperty("rates", out JsonElement resultElement))
            {
                if(resultElement.TryGetProperty(symbol.ToUpper(), out JsonElement symbolElement)) {
                    exchangeRate = (float)symbolElement.GetDouble();   
                }
            }

            return exchangeRate * amount;
        }
       
    }
}
